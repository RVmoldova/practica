<?php
include "connectDB.php";
if (!isset($_GET['prid'])) {
	include "test.php";
} else {
	$idprod = $_GET['prid'];
	$sq = "SELECT * FROM produse,furnizori,producatori WHERE (produse.idProd=producatori.id) and (produse.idFurnizor=furnizori.idFurn) and (produse.idProdus=$idprod)";
	$result = $conn->query($sq);
	if ($result->num_rows == 0) {
		echo "<center><h1>Produs inexistent</h1></center>";
		$conn->close();exit;}
	$obj = $result->fetch_object();
	$numeP = $obj->numeProdus;
	$priceP = $obj->pretProdus;
	$furnP = $obj->numeFurn;
	$prodP = $obj->producatorNume;
	$photo = $obj->photoUrl;
	$desc = $obj->descriere;
	echo "<div class=\"row\">";
	echo "<div class=\"col-md-4\">";
	echo "<img src=\"$photo\" class=\"thumbnail tbmare\"/>";
	echo "</div>";
	echo "<div class=\"col-md-6\">";
	echo "<h1>$numeP</h1>";
	echo "<p>Produs de : <strong>$prodP</strong></p>";
	echo "<p>$desc</p>";
	echo "</div>";
	echo "<div class=\"col-md-2 infobox\">";
	echo "<h1>$priceP lei</h1>";
	echo "<FORM name =\"form1\" Method =\"POST\" Action =\"#\" class=\"form\">";
	if (elogat()) {
		echo "Cantitate:<input type='number' id='numberinput' name='Cantitate' value='1' />";
		echo "<Input Type = \"Submit\" Name = \"addcos\" class=\"btn btn-default\" Value = \"Adaugă în coș\"/>";
	} else {
		echo "<span style='color:red'><strong><a href='login.php'>Logați-vă pentru a comanda</a></strong></span>";
	}
	echo "<a class=\"btn btn-default\" href=\"?tab=product\">Înapoi la listă</a>";
	echo "</FORM>";
	echo "</div>";
	echo "</div>";
}

?>

<?php
if (isset($_POST['addcos'])) {
	$iduser = $_SESSION['userid'];
	$produs = $_GET['prid'];
	$cant = $_POST['Cantitate'];
	//echo $cant . " " . $iduser . " " . $produs;
	$squery = "INSERT INTO cos (idCump,idProd,cantitate) VALUES ($iduser,$produs,$cant)";
	//echo $squery;
	if ($cant > 0) {
		if ($conn->query($squery)) {
			echo "<script type='text/javascript'>alert('Produs adăugat cu success în coș'); document.location.href = './index.php?tab=cos';</script>";
		} else {
			echo "Eroare: " . $conn->error;
		}
	} else {
		//echo "<script type='text/javascript'>alert('Cantitatea trebuie să fie mai mare ca 0'); document.location.href = '/index.php?tab=cos&prid=$produs';</script>";
		header("Location: ./index.php?tab=product");
	}
	$conn->close();
}
?>