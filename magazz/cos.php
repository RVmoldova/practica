<?php
include 'connectDB.php';
if (elogat()) {
	if (isset($_GET['del'])) {
		$prod = $_GET['del'];
		$cum = $_SESSION['userid'];
		$sq = "DELETE FROM cos WHERE idProd=$prod and idCump=$cum";
		$conn->query($sq);
	}
	$cum = $_SESSION['userid'];
	echo "<h1><center>COȘUL DUMNEAVOASTRĂ</center></h1>";
	echo "<center><a class=\"btn btn-warning\" href=\"commanda.php\" role=\"button\">COMANDĂ ACUM!</a><center>";
	$sq = "SELECT * FROM cos,produse,producatori WHERE cos.idProd=produse.idProdus and cos.idCump=$cum and produse.idProd=producatori.id";
	$i = 1;
	echo "<div class='row'><div class='col-md-1'></div><div class='col-md-10'><table class='table'><thead>";
	echo "<th>Produs</th>";
	echo "<th>Preț unitar</th>";
	echo "<th>Cantitate</th>";
	echo "<th>Preț total</th>";
	echo "<th></th>";
	echo "</thead>";
	echo "<tbody>";
	if ($result = $conn->query($sq)) {
		while ($obj = $result->fetch_object()) {
			$idprodus = $obj->idProdus;
			$produs = $obj->numeProdus;
			$pretun = $obj->pretProdus;
			$cant = $obj->cantitate;
			$producator = $obj->producatorNume;
			$total = $cant * $pretun;
			echo "<tr class=\"chatrow\">";
			echo "<td class=\"chatdata\" style=\" width: 70%; \">";
			echo "<a href=\"/index.php?tab=product&prid=$idprodus\">" . $producator . " " . $produs . "</a>";
			echo "</td>";
			echo "<td class=\"chatdata\">";
			echo $pretun;
			echo "</td>";
			echo "<td class=\"chatdata\">";
			echo $cant;
			echo "</td>";
			echo "<td class=\"chatdata\">";
			echo $total;
			echo "</td>";
			echo "<td class=\"chatdata\" style=\" width: 3%; \">";
			echo "<a href=\"./index.php?tab=cos&del=$idprodus\"><strong>X</strong></a>";
			echo "</td>";
			echo "</tr>";
			$i++;
		}
	}
	if ($i == 1) {
		echo "<tr style=\"background-color: #FF9F9F;\"><td>Nimic în coș</td><td></td><td></td><td></td></tr>";
	} else {
		$sq = "SELECT SUM(`cos`.`cantitate`*`produse`.`pretProdus`) AS `suma` FROM `produse`,`cos` WHERE `cos`.`idProd`=`produse`.`idProdus` and `cos`.`idCump`=$cum";
		$result = $conn->query($sq);
		$obj = $result->fetch_object();
		echo "<tr class=\"chatrow\">";
		echo "<td class=\"chatdata\">";
		echo "</td>";
		echo "<td class=\"chatdata\">";
		echo "</td>";
		echo "<td class=\"chatdata\" style=\"background-color: #FF9F9F;\">";
		echo "SUMA TOTALĂ:";
		echo "</td>";
		echo "<td class=\"chatdata\" style=\"background-color: #FF9F9F;\">";
		echo round($obj->suma, 2);
		echo "</td>";
		echo "<td class=\"chatdata\" style=\"background-color: #FF9F9F;\">LEI";
		echo "</td>";
		echo "</tr>";
	}
	echo "</tbody>";
	echo "</table>";
	echo "</div><div class='col-md-1'></div></div>";
} else {
	echo "insuficiente drepturi!!";
}
$conn->close();
?>