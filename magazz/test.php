<div class="row" ng-controller="MainController">
	<div class="col-md-1"></div>
	<div class="col-md-10">
		Căutare nume: <input ng-model="filtre.nume">
		Căutare producator: <input ng-model="filtre.prod">
		Sortare:
		<select ng-model="orderProp">
			<option value="nume">Alfabetical</option>
			<option value="-id">Noi</option>
			<option value="pret">Preț</option>
			<option value="-pret">Preț (descrescător)</option>
		</select>
		Categorie:
		<select ng-model="filtre.categorie">
		<option value="">-- Alege Categoria --</option>
		<option ng-repeat="i in produse | unique: 'categorie'" value="{{ i.categorie }}">{{ i.categorie }}</option>
		</select>
		<table class="table">
			<thead>
				<th>Producător</th>
				<th>Nume Produs</th>
				<th>foto</th>
				<th>Preț Produs (lei)</th>
			</thead>
			<tbody>
				<tr ng-repeat="i in produse | orderBy: orderProp | filter:filtre " class="chatrow clickableRow" ng-click="klikaneste(i.id)">
					<td class="chatdata" style="width: 10%;">{{ i.prod }}</td>
					<td class="chatdata" style="width: 74%;">{{ i.nume }}</td>
					<td class="chatdata fotochat" style="width: 5%;"><a href='{{ i.photourl }}' class='thumbnail tbmic'><img src='{{ i.photourl }}'/></a></td>
					<td class="chatdata pricedata" style="width: 11%;">{{ i.pret }}</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="col-md-1"></div>
</div>