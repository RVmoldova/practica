<?PHP
if (!isadmin()) {
	header("Location: index.php");
}
include 'connectDB.php';
$sq = "SELECT COUNT(*) AS `comenzinoi` FROM `comanda` WHERE `comanda`.`procesat`='procesare'";
$result = $conn->query($sq);
$obj = $result->fetch_object();
$comenzinoi = $obj->comenzinoi;
?>
<div class="btn-group admm center" role="group">
<table class="admintable">
<tr>
    <td><a class="btn btn-warning btn-lg">Editează:</a></td>
    <td><a class="btn btn-warning btn-lg">Management:</a></td>
    <td><a class="btn btn-warning btn-lg">Contabilitate:</a></td>
</tr>
<tr>
	<td><a class="btn btn-default btn-lg" href="addProducator.php" role="button">Producători</a></td>
	<td><a class="btn btn-default btn-lg" href="comadmin.php" role="button">Comenzi noi<?PHP if ($comenzinoi > 0) {echo "(" . $comenzinoi . ")";} else {echo " nu-s";}?></a></td>
	<td><a class="btn btn-default btn-lg" href="contabilitate.php" role="button">Raport pe Luni</a></td>
</tr>
<tr>
	<td><a class="btn btn-default btn-lg" href="addFurnizor.php" role="button">Frunizori</a></td>
	<td><a class="btn btn-default btn-lg" href="comarchive.php" role="button">Comenzi</a></td>
	<td></td>
</tr>
<tr>
	<td><a class="btn btn-default btn-lg" href="addCat.php" role="button">Categorii</a></td>
	<td></td>
	<td></td>
</tr>
<tr>
	<td><a class="btn btn-default btn-lg" href="addProdus.php" role="button">Produse</a></td>
	<td></td>
	<td></td>
</tr>
<tr>
	<td><a class="btn btn-default btn-lg" href="addPlata.php" role="button">Mod Plată</a></td>
	<td></td>
	<td></td>
</tr>
</table>
</div>