<?php
include 'fnx.php'
?>
<!DOCTYPE html>
<html ng-app="myApp" lang="ro">
<?PHP
include 'header.php';
if (!isset($_GET['tab'])) {
	gohomenna();
}
?>

	<body>
		<div class="nav glavnobar">
			<div class="container">
				<ul class="pull-left">
					<li><a href="./index.php"><div class="butonbar">Pagina principală</div></a></li>
				</ul>
				<?PHP
echo "<ul class='pull-right'>";
if (elogat()) {
	echo "<li>";
	echo "<div class='butonbar'>";
	echo "Salut,  " . $_SESSION['user'];
	echo "</div>";
	echo "</li>";
	echo "<li>";
	echo "<a href='./logout.php'>";
	echo "<div class='butonbar'>";
	echo "Logout";
	echo "</div>";
	echo "</a>";
	echo "</li>";
} else {
	echo "<li><a href='register.php'><div class='butonbar'>Înregistrare</div></a></li>";
	include 'minilogin.php';
}
echo "</ul>";
?></div>
		</div>
		<div class="jumbotron">
			<div class="container unselectable">
				<h1>InterNet magazin</h1>
				<p>proiect la PBD</p>
			</div>
		</div>
		<ul class="nav nav-tabs butoanelenav">
			<li class="<?PHP echo (($_GET['tab'] == "main") ? 'active' : '');?>"><a href="?tab=main">Principala</a></li>
			<li class="<?PHP echo (($_GET['tab'] == "product") ? 'active' : '');?>"><a href="?tab=product">Produse</a></li>
			<?PHP
if (elogat()) {
	include 'loggedmeniu.php';
}
?>
		</ul>
		<div class="infotab">
			<?PHP
if ($_GET['tab'] == "main") {
	include 'princip.php';
	////////////////////////////////////////
}
if ($_GET['tab'] == "product") {
	include 'newprod.php';
	////////////////////////////////////////
}
if ($_GET['tab'] == "cos") {
	include 'cos.php';
	////////////////////////////////////////
}
if ($_GET['tab'] == "com") {
	include 'com.php';
	////////////////////////////////////////
}

if ($_GET['tab'] == "chat") {
	if (elogat()) {
		include "chat.php";
	} else {
		echo "insuficiente drepturi";
	}
}

if ($_GET['tab'] == "admin") {
	if (isadmin()) {
		include 'admin.php';
	} else {
		echo "insuficiente drepturi";
	}
}

?>
		</div>

		<div class="footer pull-down">
				<div class="container">
					Autor: RV<br>©Chisinau 2015
				</div>
			</div>
	</body>
</html>