<?php
include 'fnx.php';
include "connectDB.php";
if (!elogat()) {
	gohomenna();
}
if (!isadmin()) {
	gohomenna();
}
?>
<!DOCTYPE html>
<html lang="ro">
	<?php include 'header.php';?>
	<body>
		<FORM name ="form1" Method ="POST" Action ="#" class="form-inline inputu no-print">
			<a class="btn btn-success" href="./index.php?tab=admin" role="button">Inapoi</a>
			<div class="form-group textcss">
				<label for="prodnume">Nume:</label>
				<Input Type = "text" Placeholder ="Numele Produsului" Name ="prodnume"  autocomplete="off" id="prodnume"/>
			</div>
			<div class="form-group textcss">
				<label for="prodpret">Preț:</label>
				<Input Type = "text" Placeholder ="Preț Produs" Name ="prodpret"  autocomplete="off" id="prodpret"/>
			</div>
			<div class="form-group textcss">
				<label for="prodid">Producator:</label>
				<select name="prodid" class="form-control">
					<?php
$sq = "SELECT * FROM producatori WHERE 1 ORDER BY producatorNume ASC";
if ($result = $conn->query($sq)) {
	while ($obj = $result->fetch_object()) {
		$idP = $obj->id;
		$numeP = $obj->producatorNume;
		echo "<option value='$idP'>";
		echo $numeP;
		echo "</option>";
		$i++;
	}
}
?>
				</select>
			</div>
			<div class="form-group textcss">
				<label for="furnid">Furnizor:</label>
				<select name="furnid" class="form-control">
					<?php
$sq = "SELECT * FROM furnizori WHERE 1 ORDER BY numeFurn ASC";
if ($result = $conn->query($sq)) {
	while ($obj = $result->fetch_object()) {
		$idP = $obj->idFurn;
		$numeP = $obj->numeFurn;
		echo "<option value='$idP'>";
		echo $numeP;
		echo "</option>";
		$i++;
	}
}
?>
				</select>
			</div>
			<div class="form-group textcss">
				<label for="prodpret">Photo URL:</label>
				<Input Type = "url" Placeholder ="http://" Name ="photo"  autocomplete="off" id="photo"/>
			</div>
			<div class="form-group textcss">
				<label for="prodpret">Stock?:</label>
				<input type="checkbox" name="stock" value="1" checked/>
			</div>
			<br>
			<div class="form-group textcss">
				<label for="prodpret">Descriere:</label>
				<Input Type = "text" Placeholder ="Descriere" size="130" Name ="desc"  autocomplete="off" id="desc"/>
			</div>
			<div class="form-group textcss">
				<label for="catid">Categorie:</label>
				<select name="catid" class="form-control">
					<?php
$sq = "SELECT * FROM categorie WHERE 1 ORDER BY idCat ASC";
if ($result = $conn->query($sq)) {
	while ($obj = $result->fetch_object()) {
		$idCat = $obj->idCat;
		$numeP = $obj->numeCat;
		echo "<option value='$idCat'>";
		echo $numeP;
		echo "</option>";
		$i++;
	}
}
?>
				</select>
			</div>
			<br>
			<Input Type = "Submit" Name = "SubmitProd" class="btn btn-success" Value = "Adaugă"/>
		</FORM>
		<div class="boxChat">
			<?php
if (isset($_POST['SubmitProd'])) {
	if (isset($_POST['stock'])) {
		$stock = 1;
	} else {
		$stock = 0;
	}
	$prodnume = $_POST['prodnume'];
	$prodid = $_POST['prodid'];
	$catid = $_POST['catid'];
	$furnid = $_POST['furnid'];
	$prodpr = $_POST['prodpret'];
	$photo = $_POST['photo'];
	$desc = str_replace("'", "\\'", $_POST['desc']);
	$squery = "INSERT INTO produse (numeProdus,pretProdus,idProd,idFurnizor,photoUrl,descriere,stock,idCategorie) VALUES ('$prodnume',$prodpr,$prodid,$furnid,'$photo','$desc',$stock,$catid)";
	//echo $squery;
	if (strlen($prodnume) > 3) {
		if ($conn->query($squery)) {
			echo "Produs adaugat cu success";
		} else {
			echo "Eroare: " . $conn->error;
		}
	} else {
		echo "Error: Numele Produsului prea scurt(MIN:4)";
	}
}
?>
			<?PHP
$sq = "SELECT * FROM produse,furnizori,producatori,categorie WHERE (produse.idProd=producatori.id) and (produse.idFurnizor=furnizori.idFurn) and (produse.idCategorie=categorie.idCat) ORDER BY idProdus ASC";
echo "<div class='row'><div class='col-md-1'></div><div class='col-md-10'><table class='table'><thead>";
echo "<th>id</th>";
echo "<th>Nume Produs</th>";
echo "<th>Preț Produs</th>";
echo "<th>Furnizor</th>";
echo "<th>Producător</th>";
echo "<th>Categorie</th>";
echo "<th>foto</th>";
echo "</thead>";
echo "<tbody>";
if ($result = $conn->query($sq)) {
	while ($obj = $result->fetch_object()) {
		$idP = $obj->idProdus;
		$numeP = $obj->numeProdus;
		$priceP = $obj->pretProdus;
		$furnP = $obj->numeFurn;
		$catid = $obj->numeCat;
		$prodP = $obj->producatorNume;
		$photo = $obj->photoUrl;
		echo "<tr class=\"chatrow clickableRow\" href=\"./editproduct.php?prid=$idP\">";
		echo "<td class=\"chatdata\" style=\" width: 13%; \">";
		echo $idP;
		echo "</td>";
		echo "<td class=\"chatdata\">";
		echo $numeP;
		echo "</td>";
		echo "<td class=\"chatdata\">";
		echo $priceP;
		echo "</td>";
		echo "<td class=\"chatdata\">";
		echo $furnP;
		echo "</td>";
		echo "<td class=\"chatdata\">";
		echo $prodP;
		echo "</td>";
		echo "<td class=\"chatdata\">";
		echo $catid;
		echo "</td>";
		echo "<td class=\"chatdata\" style=\" width: 13%; \">";
		echo "<a href='$photo' class='thumbnail tbmic'><img src='$photo'/></a>";
		echo "</td>";
		echo "</tr>";
	}
}
$conn->close();
if ($i == 1) {
	echo "<tr><td>No data</td><td>No data</td></tr>";
}
echo "</tbody>";
echo "</table>";
echo "</div><div class='col-md-1'></div></div>";
?>
</div>
</body>
</html>