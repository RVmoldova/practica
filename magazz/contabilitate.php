<?php
include 'fnx.php';
include "connectDB.php";
if (!elogat()) {
	gohomenna();
}
if (!isadmin()) {
	gohomenna();
}
?>
<!DOCTYPE html>
<html lang="ro">
<?php include 'header.php';?>
	<body>
		<FORM name ="form1" Method ="POST" Action ="#" class="form-inline inputu">
		<a class="btn btn-success no-print" href="./index.php?tab=admin" role="button">Inapoi</a>
		<a class="btn btn-success no-print" href="javascript:window.print()" role="button">print</a>
        <h1>Raport pe luni:</h1>
		</FORM>
		<div class="boxChat">
			<?PHP
$sq = "SELECT DISTINCT MONTH(`dataComanda`) AS `luna` ,YEAR(`dataComanda`) AS `anu` FROM `comanda` WHERE `procesat` NOT LIKE 'respins%' ORDER BY `anu` ASC , `luna` ASC";
echo "<div class='row'><div class='col-md-1'></div><div class='col-md-10'><table class='table'><thead>";
echo "<th>Anul</th>";
echo "<th>Luna</th>";
echo "<th>Comenzi</th>";
echo "<th>Suma Totală (lei)</th>";
echo "</thead>";
echo "<tbody>";
if ($result = $conn->query($sq)) {
	while ($obj = $result->fetch_object()) {
		$luna = $obj->luna;
		$anu = $obj->anu;
		echo "<tr class=\"chatrow\">";
		echo "<td class=\"chatdata\" style=\" width: 13%; \">";
		echo $anu;
		echo "</td>";
		echo "<td class=\"chatdata\">";
		echo $luna;
		echo "</td>";
		$sq = "SELECT COUNT(*) as `nrcomenzi` FROM `comanda` WHERE MONTH(`dataComanda`)=$luna and YEAR(`dataComanda`)=$anu and `procesat` NOT LIKE 'respins%'";
		echo "<td>" . $conn->query($sq)->fetch_object()->nrcomenzi . "</td>";
		$sq = "SELECT SUM(`continutcomanda`.`cantitate`*`produse`.`pretProdus`) AS `suma` FROM `continutcomanda`,`produse`,`comanda` WHERE `produse`.`idProdus`=`continutcomanda`.`prodId` and `comanda`.`idComanda`=`continutcomanda`.`idCom` and MONTH(`dataComanda`)=$luna and YEAR(`dataComanda`)=$anu and `procesat` NOT LIKE 'respins%'";
		echo "<td>" . round($conn->query($sq)->fetch_object()->suma, 2) . "</td>";
		echo "</tr>";
	}
}
$conn->close();
echo "</tbody>";
echo "</table>";
echo "</div><div class='col-md-1'></div></div>";
?>
</div>
</body>
</html>