app.controller('MainController', ['$scope', '$http', '$window',
	function($scope, $http, $window) {
		$http.get('./jsonprod.php').success(function(data) {
			$scope.produse = data;
		});
		$scope.orderProp = 'nume';
		$scope.klikaneste = function(id) {
			$window.location.href = '?tab=product&prid=' + id;
		}
	}
]);