<?php
include "connectDB.php";
if (!isset($_GET['prid'])) {
	if (!isset($_GET['ord'])) {
		header("Location: ./index.php?tab=product&cat=0&ord=name");
	} else {
		$ord = $_GET['ord'];
		if ($ord == 'name') {
			$order = "  ORDER BY numeProdus ASC";
		} else if ($ord == 'named') {
			$order = "  ORDER BY numeProdus DESC";
		} else
		if ($ord == 'prod') {
			$order = "  ORDER BY producatorNume ASC";
		} else if ($ord == 'prodd') {
			$order = "  ORDER BY producatorNume DESC";
		} else
		if ($ord == 'pret') {
			$order = "  ORDER BY pretProdus ASC";
		} else if ($ord == 'pretd') {
			$order = "  ORDER BY pretProdus DESC";
		} else {
			$order = "  ORDER BY numeProdus ASC";
		}
	}
	if (!isset($_GET['cat'])) {
		header("Location: ./index.php?tab=product&cat=0&ord=name");
	}
	$curcat = $_GET['cat'];
	$curord = $_GET['ord'];
	echo "<div class='row'><div class='col-md-1'></div><div class='col-md-10'><br><br><center>";
	$sq = "SELECT * FROM categorie WHERE idCat<>0";
	if ($result = $conn->query($sq)) {
		while ($obj = $result->fetch_object()) {
			$numecat = $obj->numeCat;
			$idcat = $obj->idCat;
			$apcat = $curcat == $idcat ? "apasatCat" : "";
			if ($idcat == $curcat) {
				$idcat = 0;
			}
			echo "<a href='./index.php?tab=product&cat=$idcat&ord=$curord' class='butonCat $apcat'>$numecat</a>";
		}
	}
	echo "</center></div><div class='col-md-1'></div></div>";
	echo "<div class=\"boxChat\">";
	if ($curcat != 0) {
		$catq = "and (idCategorie=$curcat)";
	} else {
		$catq = "";
	}
	$sq = "SELECT * FROM produse,furnizori,producatori WHERE (produse.idProd=producatori.id) and (produse.idFurnizor=furnizori.idFurn) and (produse.stock=1) $catq" . $order;
	echo "<div class='row'><div class='col-md-1'></div><div class='col-md-10'><table class='table'><thead>";
	$ord = $_GET['ord'];
	$n0 = ($ord == 'prod' ? 'd' : '');
	$n1 = ($ord == 'name' ? 'd' : '');
	$n2 = ($ord == 'pret' ? 'd' : '');
	$g0 = ($ord == 'prod' ? 'glyphicon glyphicon-sort-by-attributes' : ($ord == 'prodd' ? 'glyphicon glyphicon-sort-by-attributes-alt' : ''));
	$g1 = ($ord == 'name' ? 'glyphicon glyphicon-sort-by-attributes' : ($ord == 'named' ? 'glyphicon glyphicon-sort-by-attributes-alt' : ''));
	$g2 = ($ord == 'pret' ? 'glyphicon glyphicon-sort-by-attributes' : ($ord == 'pretd' ? 'glyphicon glyphicon-sort-by-attributes-alt' : ''));
	echo "<th><a href='?tab=product&cat=$curcat&ord=prod$n0'>Producător</a> <span class='$g0' aria-hidden='true'></span></th>";
	echo "<th><a href='?tab=product&cat=$curcat&ord=name$n1'>Nume Produs</a> <span class='$g1' aria-hidden='true'></span></th>";
	echo "<th>foto</th>";
	echo "<th><a href='?tab=product&cat=$curcat&ord=pret$n2'>Preț Produs (lei)</a> <span class='$g2' aria-hidden='true'></span></th>";
	echo "</thead>";
	echo "<tbody>";
	if ($result = $conn->query($sq)) {
		while ($obj = $result->fetch_object()) {
			$idP = $obj->idProdus;
			$numeP = $obj->numeProdus;
			$priceP = $obj->pretProdus;
			$prodP = $obj->producatorNume;
			$photo = $obj->photoUrl;
			echo "<tr class=\"chatrow clickableRow\" href=\"?tab=product&prid=$idP\">";
			echo "<td class=\"chatdata\" style=\" width: 10%; \">";
			echo $prodP;
			echo "</td>";
			echo "<td class=\"chatdata\" style=\" width: 74%; \">";
			echo $numeP;
			echo "</td>";
			echo "<td class=\"chatdata fotochat\" style=\" width: 5%; \">";
			echo "<a href='$photo' class='thumbnail tbmic'><img src='$photo'/></a>";
			echo "</td>";
			echo "<td class=\"chatdata pricedata\" style=\" width: 11%; \">";
			echo $priceP;
			echo "</td>";
			echo "</tr>";
		}
	}
	$conn->close();
	echo "</tbody>";
	echo "</table>";
	echo "</div><div class='col-md-1'></div></div>";
	echo "</div>";
} else {
	$idprod = $_GET['prid'];
	$sq = "SELECT * FROM produse,furnizori,producatori WHERE (produse.idProd=producatori.id) and (produse.idFurnizor=furnizori.idFurn) and (produse.idProdus=$idprod)";
	$result = $conn->query($sq);
	if ($result->num_rows == 0) {
		echo "<center><h1>Produs inexistent</h1></center>";
		$conn->close();exit;}
	$obj = $result->fetch_object();
	$numeP = $obj->numeProdus;
	$priceP = $obj->pretProdus;
	$furnP = $obj->numeFurn;
	$prodP = $obj->producatorNume;
	$photo = $obj->photoUrl;
	$desc = $obj->descriere;
	echo "<div class=\"row\">";
	echo "<div class=\"col-md-4\">";
	echo "<img src=\"$photo\" class=\"thumbnail tbmare\"/>";
	echo "</div>";
	echo "<div class=\"col-md-6\">";
	echo "<h1>$numeP</h1>";
	echo "<p>Produs de : <strong>$prodP</strong></p>";
	echo "<p>$desc</p>";
	echo "</div>";
	echo "<div class=\"col-md-2 infobox\">";
	echo "<h1>$priceP lei</h1>";
	echo "<FORM name =\"form1\" Method =\"POST\" Action =\"#\" class=\"form\">";
	if (elogat()) {
		echo "Cantitate:<input type='number' id='numberinput' name='Cantitate' value='1' />";
		echo "<Input Type = \"Submit\" Name = \"addcos\" class=\"btn btn-default\" Value = \"Adaugă în coș\"/>";
	} else {
		echo "<span style='color:red'><strong><a href='login.php'>Logați-vă pentru a comanda</a></strong></span>";
	}
	echo "<a class=\"btn btn-default\" href=\"?tab=product\">Înapoi la listă</a>";
	echo "</FORM>";
	echo "</div>";
	echo "</div>";
}

?>

<?php
if (isset($_POST['addcos'])) {
	$iduser = $_SESSION['userid'];
	$produs = $_GET['prid'];
	$cant = $_POST['Cantitate'];
	//echo $cant . " " . $iduser . " " . $produs;
	$squery = "INSERT INTO cos (idCump,idProd,cantitate) VALUES ($iduser,$produs,$cant)";
	//echo $squery;
	if ($cant > 0) {
		if ($conn->query($squery)) {
			echo "<script type='text/javascript'>alert('Produs adăugat cu success în coș'); document.location.href = './index.php?tab=cos';</script>";
		} else {
			echo "Eroare: " . $conn->error;
		}
	} else {
		//echo "<script type='text/javascript'>alert('Cantitatea trebuie să fie mai mare ca 0'); document.location.href = '/index.php?tab=cos&prid=$produs';</script>";
		header("Location: ./index.php?tab=product");
	}
	$conn->close();
}
?>