#include <fstream>
#include <iostream>
#include <algorithm>
#include <set>
#define N_MAX 21000
std::ifstream fin("pubele.in");
std::ofstream fout("pubele.out");


int pube[N_MAX];
std::multiset<int> set1;
std::multiset<int> set2;

int n, maxp;
int rs = 0;
int check(std::multiset<int> a, std::multiset<int> b) {
	std::multiset<int>::iterator it1 = a.begin();
	std::multiset<int>::iterator it2 = b.begin();
	int ret = 0;
	while (it1 != a.end()) {
		if (*it1 < *it2)ret++;
		else return 0;
		it1++;
		it2++;
	}
	return ret;
}

int main() {
	fin >> maxp >> n;
	for (int i = 1; i <= n; ++i)
	{
		fin >> pube[i];
	}
	
	set1.insert(pube[1]);
	set2.insert(pube[2]);
	int last2 = 2;
	int ll = 3;

	for (int i = 1; i <= n / 2; ++i)
	{
		// set1.clear();
		// for (int a = 1; a <= last2; a++) {
		// 	set1.insert(pube[a]);
		// }
		// set2.clear();
		// for (int a = last2 + 1; a <= last2 + last2; a++) {
		// 	set2.insert(pube[a]);
		// }
		// for (auto a : set1) {
		// 	fout << a << " ";
		// }
		// fout << "\n";
		// for (auto a : set2) {
		// 	fout << a << " ";
		// }
		// fout << "\n---------------" << last2 << "\n";
		int sol = check(set1, set2);
		if (sol > rs)rs = sol;
		auto it = set2.lower_bound(pube[last2]);
		set2.erase(it);
		set1.insert(pube[last2]);
		last2++;
		set2.insert(pube[ll]);
		set2.insert(pube[ll + 1]);
		ll += 2;
	}
	std::cout << rs << "\n";

	return 0;
}

// 1
// 2

// 1 2
// 3 4

// 1 2 3
// 4 5 6

// 1 2 3 4
// 5 6 7 8

// 1 2 3 4 5 6 7 8
// . ,               2
// . . , ,           3
// . . . , , ,       4
// . . . . , , , ,   5
