#include <iostream>
#include <set>
#define pereche std::pair<int,int>
#define f first
#define s second

std::multiset<pereche> arb;

int main(int argc, char const *argv[])
{
	arb.insert(std::make_pair(3, 8));
	arb.insert(std::make_pair(1, 3));
	arb.insert(std::make_pair(5, 8));
	arb.insert(std::make_pair(8, 10));
	for (std::multiset<pereche>::iterator it = arb.lower_bound(std::make_pair(4, 4)); it != arb.upper_bound(std::make_pair(9, 9)); it++) {
		std::cout << it->f << " " << it->s << "\n";
	}

	return 0;
}