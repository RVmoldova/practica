#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <iostream>


std::ifstream fin("cuart.in");
std::ofstream fout("cuart.out");

int freacaimpare(int nr) {
	std::stringstream ss;
	ss.str("");
	ss << nr;
	std::string s = ss.str();
	ss.str("");
	int l = 0;
	for (unsigned int i = 0; i < s.length(); ++i) {
		if (((int)s[i] - 48) % 2 == 0) {
			ss << s[i];
			l++;
		}
	}
	if (!l) return 0;
	int ret;
	ss >> ret;
	return ret;
}

int freacapare(int nr) {
	std::stringstream ss;
	ss.str("");
	ss << nr;
	std::string s = ss.str();
	ss.str("");
	int l = 0;
	for (unsigned int i = 0; i < s.length(); ++i) {
		if (((int)s[i] - 48) % 2 == 1) {
			ss << s[i];
			l++;
		}
	}
	if (!l) return 0;
	int ret;
	ss >> ret;
	return ret;
}

bool isQuartz(int nr) {
	if (nr == 0) return 0;
	int s = 0;
	int i = 1;
	while (s < nr) {
		s += i;
		i += 4;
	}
	if (s == nr) return 1; else return 0;
}

std::vector<int> cartonaseGina;
std::vector<int> cartonaseMihai;

int main()
{
	int P;
	fin >> P;
	int N;
	fin >> N;

	for (int i = 1; i <= N; ++i) {
		int nr;
		fin >> nr;
		cartonaseGina.push_back(nr);
	}

	for (int i = 1; i <= N; ++i) {
		int nr;
		fin >> nr;
		cartonaseMihai.push_back(nr);
	}

	if (P == 1) {

		int max = 0;
		for (std::vector<int>::iterator it = cartonaseGina.begin(); it != cartonaseGina.end(); ++it) {
			if (freacapare(*it) == 0) {
				if (*it > max) max = *it;
			}
		}
		for (std::vector<int>::iterator it = cartonaseMihai.begin(); it != cartonaseMihai.end(); ++it) {
			if (freacaimpare(*it) == 0) {
				if (*it > max) max = *it;
			}
		}
		fout << max << "\n";

	} else if (P == 2) {

		int qGina, qMihai;
		qGina = qMihai = 0;
		for (std::vector<int>::iterator it = cartonaseGina.begin(); it != cartonaseGina.end(); ++it) {
			if (isQuartz(freacapare(*it))) qGina++;
		}
		for (std::vector<int>::iterator it = cartonaseMihai.begin(); it != cartonaseMihai.end(); ++it) {
			if (isQuartz(freacaimpare(*it))) qMihai++;
		}
		if (qGina > qMihai) {
			fout << "1\n";
		} else if (qMihai > qGina) {
			fout << "2\n";
		} else {
			if (freacapare(*cartonaseGina.begin()) > freacaimpare(*cartonaseMihai.begin())) {
				fout << "1\n";
			} else if (freacapare(*cartonaseGina.begin()) < freacaimpare(*cartonaseMihai.begin())) {
				fout << "2\n";
			} else fout << "0\n";
		}

	} else {

		int qGina, qMihai;
		qGina = qMihai = 0;
		for (std::vector<int>::iterator it = cartonaseGina.begin(); it != cartonaseGina.end(); ++it) {
			if (isQuartz(freacapare(*it))) qGina++;
		}
		for (std::vector<int>::iterator it = cartonaseMihai.begin(); it != cartonaseMihai.end(); ++it) {
			if (isQuartz(freacaimpare(*it))) qMihai++;
		}
		if (qGina > qMihai) {
			fout << qGina << "\n";
		} else if (qMihai > qGina) {
			fout << qMihai << "\n";
		} else {
			if (*cartonaseGina.begin() > *cartonaseMihai.begin()) {
				fout << freacapare(*cartonaseGina.begin()) << "\n";
			} else if (*cartonaseGina.begin() < *cartonaseMihai.begin()) {
				fout << freacaimpare(*cartonaseMihai.begin()) << "\n";
			} else fout << "0\n";
		}

	}


	return 0;
}
