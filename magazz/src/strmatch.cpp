#include <fstream>
#include <string>
#include <queue>

std::ifstream fin("strmatch.in");
std::ofstream fout("strmatch.out");

std::string ac, hatch;
std::queue<std::size_t> rs;



int main() {

	fin >> ac >> hatch;

	std::size_t pos = -1;
	int p = 0;
	while (1) {
		pos = hatch.find(ac, pos + 1);
		if (pos == std::string::npos) break;
		else {
			p++;
			if (p <= 1000)
				//	fout<<p<<"s\n";
				rs.push(pos);
		}
	}
	fout << p << "\n";
	while (!rs.empty()) {
		fout << rs.front() << " ";
		rs.pop();
	}
	fout << "\n";
	return 0;
}
