#include <bits/stdc++.h>
#define DjSemn std::pair<bool,char>

std::ifstream fin("DJ.in");
std::ofstream fout("DJ.out");

std::map<char, std::map<char, DjSemn> > mapa;

void initMap() {
	mapa['1']['1'] = std::make_pair(0, '1');
	mapa['1']['i'] = std::make_pair(0, 'i');
	mapa['1']['j'] = std::make_pair(0, 'j');
	mapa['1']['k'] = std::make_pair(0, 'k');

	mapa['i']['1'] = std::make_pair(0, 'i');
	mapa['i']['i'] = std::make_pair(1, '1');
	mapa['i']['j'] = std::make_pair(0, 'k');
	mapa['i']['k'] = std::make_pair(1, 'j');

	mapa['j']['1'] = std::make_pair(0, 'j');
	mapa['j']['i'] = std::make_pair(1, 'k');
	mapa['j']['j'] = std::make_pair(1, '1');
	mapa['j']['k'] = std::make_pair(0, 'i');

	mapa['k']['1'] = std::make_pair(0, 'k');
	mapa['k']['i'] = std::make_pair(0, 'j');
	mapa['k']['j'] = std::make_pair(1, 'i');
	mapa['k']['k'] = std::make_pair(1, '1');
}

DjSemn prodCh(DjSemn a, DjSemn b) {
	bool semn;
	if (a.first == b.first) {
		semn = 0;
	} else semn = 1;
	DjSemn c = mapa[a.second][b.second];
	if (c.first == semn) {
		semn = 0;
	} else semn = 1;
	return std::make_pair(semn, c.second);
}

int main() {
	initMap();
	// DjSemn a = std::make_pair(0, 'j');
	// DjSemn b = std::make_pair(0, 'i');
	// DjSemn c = prodCh(prodCh(a, b), a);
	//std::cout << (c.first ? "-" : "") << c.second << "\n";
	int TEST;
	fin >> TEST;

	for (int T = 1; T <= TEST; T++) {
		std::string s, si;
		int l, x;
		fin >> l >> x >> si;
		for (int i = 1; i <= x; ++i) {
			s += si;
		}
		DjSemn cur = std::make_pair(0, '1');
		std::queue<int> stI;
		std::queue<int> stJ;
		for (unsigned int i = 0; i < s.length(); i++) {
			cur = prodCh(cur, std::make_pair(0, s[i]));
			if (cur.first == 0 && cur.second == 'i') {
				stI.push(i);
				//break;
			}
			//fout << i << ": " << (cur.first ? "-" : "") << cur.second << "\n";
		}
		while (!stI.empty()) {
			cur = std::make_pair(0, '1');
			int fr = stI.front();
			stI.pop();
			for (unsigned int i = fr + 1; i < s.length(); i++) {
				cur = prodCh(cur, std::make_pair(0, s[i]));
				if (cur.first == 0 && cur.second == 'j') {
					stJ.push(i);
					//	break;
				}
				//fout << i << ": " << (cur.first ? "-" : "") << cur.second << "\n";
			}
		}
		while (!stI.empty()) {
			stJ.pop();
		}
		bool found = 0;
		while (!stJ.empty()) {
			cur = std::make_pair(0, '1');
			int fr = stJ.front();
			stJ.pop();
			for (unsigned int i = fr + 1; i < s.length(); i++) {
				cur = prodCh(cur, std::make_pair(0, s[i]));
				if (cur.first == 0 && cur.second == 'k' && (i == s.length() - 1)) {
					found = 1;
					break;
				}
				//fout << i << ": " << (cur.first ? "-" : "") << cur.second << "\n";
			}
		}
		while (!stJ.empty()) {
			stJ.pop();
		}
		std::cout << "Case #" << T << ": " << (found ? "YES" : "NO") << "\n";
		fout << "Case #" << T << ": " << (found ? "YES" : "NO") << "\n";
	}
	return 0;
}