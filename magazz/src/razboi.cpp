#include <iostream>
#include <fstream>
#include <limits>
#define NMAX 510
std::ifstream fin("razboi.in");
std::ofstream fout("razboi.out");
int W[NMAX + 5][NMAX + 5];
int n, m, x;
int maxes[NMAX + 5];
int main()
{
	fin >> n >> m;
	for (int i = 0; i <= NMAX; ++i) {
		for (int j = 0; j <= NMAX; ++j)
		{
			W[i][j] = 999999999;
		}
	}
	for (int i = 0; i < m; ++i)
	{
		int a, b, w;
		fin >> a >> b >> w;
		W[a][b] = w;
	}
	fin >> x;
	for (int k = 1; k <= n; ++k) {
		for (int i = 1; i <= n; ++i) {
			for (int j = 1; j <= n; ++j) {
				if (W[i][k] + W[k][j] < W[i][j])
					W[i][j] = W[i][k] + W[k][j];
			}
		}
	}
	int miny = 9999999;
	int y = 0;
	for (int i = 1; i <= n; ++i)
	{
		int tmax = 0;
		for (int j = 1; j <= n; ++j) {
			if (i != j) {
				if (W[j][i] > tmax) tmax = W[j][i];
			}
		}
		maxes[i] = tmax;
		if (tmax < miny) {
			miny = tmax;
			y = i;
		}
	}
	fout << (maxes[x] >= 999999999 ? 0 : maxes[x]) << "\n" << y << " ";
	if (y != 0) {fout << miny;}
	fout << "\n";
	return 0;
}
