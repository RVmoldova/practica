#include <iostream>
#include <string>

///////////// Clasa figura ////////////////
class Figura {
protected:
	int inaltime;
	int latimea;
public:
	Figura();
	~Figura();
	void seteazaInaltimea(int);
	void seteazaLatimea(int);
};
////////////////////////////////////////////



///////////// Clasa Dreptunghi (derivat de la Figura) ////////////////
class Dreptunghi: public Figura {
public:
	Dreptunghi();
	~Dreptunghi();
	int returnArie();
};
///////////////////////////////////////////////



///////////// Clasa DreptunghiColorat (derivat de la Dreptunghi) ////////////////
class DreptunghiColorat: public Dreptunghi {
protected:
	std::string culoarea;
public:
	DreptunghiColorat(std::string);
	~DreptunghiColorat();
	std::string returnCuloare();
};
///////////////////////////////////////////////





//////////////////////////////  MAIN
int main()
{

	DreptunghiColorat dr("Verde");

	dr.seteazaInaltimea(20);

	dr.seteazaLatimea(32);

	std::cout << "\nDreptunghi de culoarea " << dr.returnCuloare() << " are aria de " << dr.returnArie() << "\n\n";

	return 0;
}
//////////////////////////////





//////////////////////////////////////////////////

//                    Implementarea


////////////////////////////////////////////////////
//vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv//


/////////////////////////////////////////////////    Membrii Figura
Figura::Figura() {
	std::cout << "Apelare Constructor Clasa Figura\n";
	inaltime = latimea = 0;
}

Figura::~Figura() {
	std::cout << "Apelare Destructor Clasa Figura\n";
}

void Figura::seteazaInaltimea(int h) {
	inaltime = h;
}

void Figura::seteazaLatimea(int w) {
	latimea = w;
}
/////////////////////////////////////////////////




/////////////////////////////////////////////////  MEMBRII Dreptunghi
Dreptunghi::Dreptunghi() {
	std::cout << "Apelare Constructor Clasa Dreptunghi\n";
}
Dreptunghi::~Dreptunghi() {
	std::cout << "Apelare Destructor Clasa Dreptunghi\n";
}

int Dreptunghi::returnArie() {
	return inaltime * latimea;
}
/////////////////////////////////////////////////




/////////////////////////////////////////////////  MEMBRII DreptunghiColorat
DreptunghiColorat::DreptunghiColorat(std::string c) {
	std::cout << "Apelare Constructor Clasa DreptunghiColorat\n";
	culoarea = c;
}
DreptunghiColorat::~DreptunghiColorat() {
	std::cout << "Apelare Destructor Clasa DreptunghiColorat\n";
}

std::string DreptunghiColorat::returnCuloare() {
	return culoarea;
}
/////////////////////////////////////////////////
