#include <fstream>
#include <vector>
#include <queue>
#include <map>
#include <functional>

std::ifstream fin("graf.in");
std::ofstream fout("graf.out");

std::queue<int> coada;
std::vector<int> v[7510];
std::map<int, int> mapa;
std::priority_queue<int, std::vector<int>, std::greater<int> > answer;
int N, M, X, Y;

bool viz[7510];
int dist[7510];
int main()
{
	fin >> N >> M >> X >> Y;

	for (int i = 0; i < M; ++i)
	{
		int a, b;
		fin >> a >> b;
		v[a].push_back(b);
		v[b].push_back(a);
	}

	coada.push(X);
	int front;
	//int drummin = 0;
	answer.push(X);

	viz[X] = 1;
	dist[X] = 0;
	while (!coada.empty()) {
		front = coada.front();
		coada.pop();
		mapa[front]++;
		for (std::vector<int>::iterator it = v[front].begin(); it != v[front].end(); it++) {
			if (!viz[*it]) {
				coada.push(*it);
				dist[*it] = dist[front] + 1;
				//mapa[*it]++;
				viz[*it] = 1;
			}
			if (dist[front] + 1 <= dist[*it]) {
				coada.push(*it);
				//mapa[*it]++;
				dist[*it] = dist[front] + 1;
			}
		}
	}
	// for (int i = 1; i <= N; ++i) {
	// 	fout << dist[i] << " ";
	// }



	int i = Y;
	bool nonono = 0;
	while (1) {
		answer.push(i);
		bool ad = 1;
		for (std::vector<int>::iterator it = v[i].begin(); it != v[i].end(); it++) {
			if (mapa[*it] + 1 == mapa[i]) {
				i = *it;
				ad = 0;
				if (*it == 1) {
					nonono = 1;
				}
				break;
			}
		}
		if (ad) {
			break;
		}
	}
	if (nonono) {
		answer.pop();
	}
	fout << answer.size() << "\n";
	while (!answer.empty()) {
		fout << answer.top() << " ";
		answer.pop();
	}
	fout << "\n";
// coada.push(Y);
// viz = new bool[N + 10]();
// dist = new int[N + 10]();
// viz[Y] = 1;
// dist[Y] = 0;
// while (!coada.empty()) {
// 	front = coada.front();
// 	coada.pop();
// 	for (std::vector<int>::iterator it = v[front].begin(); it != v[front].end(); it++) {
// 		if (!viz[*it]) {
// 			coada.push(*it);
// 			dist[*it] = dist[front] + 1;
// 			viz[*it] = 1;
// 		}
// 		if (dist[front] + 1 < dist[*it]) {
// 			coada.push(*it);
// 			dist[*it] = dist[front] + 1;
// 		}
// 	}
// }
// for (int i = 1; i <= N; ++i) {
// 	fout << dist[i] << " ";
// }
// delete[] viz;
// delete[] dist;

	return 0;
}
