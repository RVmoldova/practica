#include <iostream>

#define DEBUG 1   // 1 - pentru afisarea mesajelor de la operatori/constructori/destructori



///////////////////////////////////////////
/////////Declararea clasei Homie///////////
///////////////////////////////////////////

class Homie {
private:
	int money;
public:
	Homie();                           //Constructor fara parametri
	Homie(int);                        //Constructor cu parametru
	Homie(const Homie&);               //Constructor de copiere
	~Homie();                          //Destructor
	void showMeYaMoney();              //membru ce afiseaza valoarea "money"
	void operator= (const Homie&);     //supraincarcarea operatorului de atribuire
	Homie operator+ (const Homie&);    //supraincarcarea operatorului de adunare
};

///////////////////////////////////////////
///////////////////////////////////////////
///////////////////////////////////////////


int main()
{
	Homie homie1 = Homie();         //folosirea I constructor (fara parametri)
	Homie homie2 = Homie(230);      //folosirea II constructor ( cu parametri)
	Homie homie3 = Homie(homie2);   //folosirea constructorului de copiere
	homie3 = homie2 + homie2;       //folosirea operatorului de atribuire si de adunare
	homie1.showMeYaMoney();         //
	homie2.showMeYaMoney();         // > apelarea membrului showMeYaMoney();
	homie3.showMeYaMoney();         //
	return 0;
}





/////////////////////////////////////////////////////////
//////Implementarea membrilor clasei Homie///////////////
/////////////////////////////////////////////////////////

Homie::Homie() {
	if (DEBUG)std::cout << "apelare Constructor fara paramatri\n";
	money = 0;
}


Homie::Homie(int money) {
	if (DEBUG)std::cout << "apelare Constructor cu parametru\n";
	this->money = money;
}

Homie::Homie(const Homie& cp) {
	if (DEBUG)std::cout << "apelare Constructor de copiere\n";
	this->money = cp.money;
}

Homie::~Homie() {
	if (DEBUG)std::cout << "Destructor\n";
}

void Homie::showMeYaMoney() {
	std::cout << "This homie has: " << this->money << " money\n";
}

void Homie::operator= (const Homie& op) {
	if (DEBUG)std::cout << "Apelare operator de atribuire\n";
	this->money = op.money;
}

Homie Homie::operator+ (const Homie& op) {
	if (DEBUG)std::cout << "Apelare operator de adunare\n";
	int temp = this->money + op.money;
	return Homie(temp);
}

/////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////
