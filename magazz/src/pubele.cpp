#include <fstream>
#include <iostream>
#include <algorithm>
#define N_MAX 10000
std::ifstream fin("pubele.in");
std::ofstream fout("pubele.out");
struct puba {
	int greutatea;
	bool luat;
	puba() {
		luat = 0;
	}
	bool operator<(puba a) {
		return this->greutatea > a.greutatea;
	}
};

puba pube[N_MAX];

int n, maxp;
int main() {
	fin >> maxp >> n;
	for (int i = 1; i <= n; ++i)
	{
		fin >> pube[i].greutatea;
	}

	int linia = n / 2;

	std::sort(pube + 1 + linia, pube + 1 + n);
	int rs = 0;
	for (int i = 1; i <= n ; ++i)
	{
		if (linia + 1 == i) {
			fout << "\n";
		}
		fout << pube[i].greutatea << " ";
	}
	fout << "\n\n\n";
	for (int i = 1; i <= linia; ++i) {
		bool loh = 1;
		for (int j = n; j > linia; --j) {
			if (!pube[j].luat) {
				if (pube[i].greutatea < pube[j].greutatea) {
					pube[j].luat = 1;
					fout << pube[i].greutatea << " " << pube[j].greutatea << "\n";
					rs++;
					loh = 0;
					break;
				}
			}
		}
		if (loh) {
			linia--;
			std::sort(pube + 1 + linia, pube + 1 + n);
			i--;
		}
		if (linia == i - 1) {
			break;
		}
	}
	std::cout << rs;


	return 0;
}
