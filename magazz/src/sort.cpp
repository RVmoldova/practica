#include <fstream>
#include <algorithm>
#include <vector>

std::ifstream fin("algsort.in");
std::ofstream fout("algsort.out");

std::vector<int> v;
int n;

int main()
{
	fin>>n;
	for (int i = 0; i < n; ++i)
	{
		int a;
		fin>>a;
		v.push_back(a);
		std::push_heap(v.begin(), v.end());
	}
	std::sort_heap(v.begin(), v.end());
	for(std::vector<int>::iterator it=v.begin();it!=v.end();it++){
		fout<<*it<<" ";
	}

	return 0;
}