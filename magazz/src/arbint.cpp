#include <iostream>

int arb[1000];


void insert(int nod, int st, int dr, int a, int b, int el) {
	if (a <= st && dr <= b) {
		arb[nod] += el;
	} else {
		int mij = (st + dr) / 2;
		if (a <= mij)
			insert(nod * 2 + 1, st, mij, a, b, el);
		if (b > mij)
			insert(nod * 2 + 2, mij + 1, dr, a, b, el);
		arb[nod] = arb[nod * 2 + 1] + arb[nod * 2 + 2];
	}
}

int sum(int nod, int st, int dr, int a, int b) {
	if (a <= st && dr <= b) {
		return	arb[nod];
	} else {
		int mij = (st + dr) / 2;
		int as, ad;
		as = ad = 0;
		if (a <= mij)
			as = sum(nod * 2 + 1, st, mij, a, b);
		if (b > mij)
			ad = sum(nod * 2 + 2, mij + 1, dr, a, b);
		return (as + ad);
	}
}



int main()
{

	for (int i = 1; i <= 10; ++i) {
		insert(0, 1, 10, i, i, i);
	}

	for (int i = 0; i <= 30; ++i)
	{
		std::cout << arb[i] << " ";
	}
	std::cout<<"\n"<<sum(0,1,10,3,5);
	return 0;
}