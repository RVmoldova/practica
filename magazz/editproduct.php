<?php
include 'fnx.php';
include "connectDB.php";
if (!elogat()) {
	gohomenna();
}
if (!isadmin()) {
	gohomenna();
}
$curp = $_GET['prid'];
if (isset($_GET['del'])) {
	$sq = "DELETE FROM produse WHERE idProdus=$curp";
	if ($conn->query($sq)) {
		echo "<script type='text/javascript'>alert('Produs șters cu success'); document.location.href = './addProdus.php';</script>";
		exit;
	} else {
		echo "<script type='text/javascript'>alert('eroare la ștergere'); document.location.href = './addProdus.php';</script>";
	}
}
$sq = "SELECT * FROM produse WHERE idProdus=$curp";
if ($result = $conn->query($sq)) {
	$obj = $result->fetch_object();
	$curprod = $obj->idProd;
	$curfurn = $obj->idFurnizor;
	$curdesc = $obj->descriere;
	$curphoto = $obj->photoUrl;
	$curpret = $obj->pretProdus;
	$curnume = $obj->numeProdus;
	$curcat = $obj->idCategorie;
	$stock = $obj->stock;
}
?>
<?php include 'header.php';?>
<FORM name ="form1" Method ="POST" Action ="#" class="form">
	<a class="btn btn-success" href="./addProdus.php" role="button">Inapoi</a>
	<div class="form-group textcss">
		<label for="prodnume">Nume:</label>
		<Input Type = "text" class="form-control" Placeholder ="Numele Produsului" Name ="prodnume" value="<?PHP echo $curnume?>"  autocomplete="off" id="prodnume"/>
	</div>
	<div class="form-group textcss">
		<label for="prodpret">Preț:</label>
		<Input Type = "text" class="form-control" Placeholder ="Preț Produs" value="<?PHP echo $curpret?>" Name ="prodpret"  autocomplete="off" id="prodpret"/>
	</div>
	<div class="form-group textcss">
		<label for="prodid">Producator:</label>
		<select name="prodid" class="form-control">
			<?php
$sq = "SELECT * FROM producatori WHERE 1 ORDER BY id ASC";
if ($result = $conn->query($sq)) {
	while ($obj = $result->fetch_object()) {
		$idP = $obj->id;
		$numeP = $obj->producatorNume;
		$acts = ($curprod == $idP ? 'selected=\"selected\"' : '');
		echo "<option value='$idP' $acts >";
		echo $numeP;
		echo "</option>";
		$i++;
	}
}
?>
		</select>
	</div>
	<div class="form-group textcss">
		<label for="furnid">Furnizor:</label>
		<select name="furnid" class="form-control">
			<?php
$sq = "SELECT * FROM furnizori WHERE 1 ORDER BY idFurn ASC";
if ($result = $conn->query($sq)) {
	while ($obj = $result->fetch_object()) {
		$idP = $obj->idFurn;
		$numeP = $obj->numeFurn;
		$acts = ($curfurn == $idP ? 'selected=\"selected\"' : '');
		echo "<option value='$idP' $acts>";
		echo $numeP;
		echo "</option>";
		$i++;
	}
}
?>
		</select>
			<div class="form-group textcss">
		<label for="catid">Categorie:</label>
		<select name="catid" class="form-control">
			<?php
$sq = "SELECT * FROM categorie WHERE 1 ORDER BY idCat ASC";
if ($result = $conn->query($sq)) {
	while ($obj = $result->fetch_object()) {
		$idC = $obj->idCat;
		$numeC = $obj->numeCat;
		$acts = ($curcat == $idC ? 'selected=\"selected\"' : '');
		echo "<option value='$idC' $acts>";
		echo $numeC;
		echo "</option>";
		$i++;
	}
}
?>
		</select>
	</div>
	<div class="form-group textcss">
		<label for="prodpret">Photo URL:</label>
		<Input Type = "url" class="form-control" Placeholder ="link" Name ="photo" value="<?PHP echo $curphoto?>" autocomplete="off" id="photo"/>
	</div>
	<div class="form-group textcss">
		<label for="prodpret">Stock?:</label>
		<input type="checkbox" name="stock" value="1" <?PHP if ($stock) {echo 'checked';}?>/>
	</div>
	<br>
	<div class="form-group textcss">
		<label for="prodpret">Descriere:</label>
		<Input Type = "text" class="form-control" Placeholder ="Descriere" size="130" Name ="desc" value="<?PHP echo htmlentities($curdesc)?>" autocomplete="off" id="desc"/>
	</div>
	<br>
	<Input Type = "Submit" Name = "SubmitProd" class="btn btn-success" Value = "Editează"/>
	<a class="btn btn-danger" href="<?PHP echo 'editproduct.php?prid=' . $curp . '&del=1'?>" role="button">Șterge</a>
</FORM>
<?php
if (isset($_POST['SubmitProd'])) {
	if (isset($_POST['stock'])) {
		$stock = 1;
	} else {
		$stock = 0;
	}
	$prodnume = $_POST['prodnume'];
	$prodid = $_POST['prodid'];
	$furnid = $_POST['furnid'];
	$prodpr = $_POST['prodpret'];
	$photo = $_POST['photo'];
	$catid = $_POST['catid'];
	$desc = str_replace("'", "\\'", $_POST['desc']);
	$squery = "UPDATE produse SET numeProdus='$prodnume',pretProdus=$prodpr,idProd=$prodid,idFurnizor=$furnid,photoUrl='$photo',descriere='$desc',stock='$stock',idCategorie=$catid WHERE idProdus=$curp";
	//echo $squery;
	if (strlen($prodnume) > 3) {
		if ($conn->query($squery)) {
			echo "<script type='text/javascript'>alert('Produs editat cu success'); document.location.href = './addProdus.php';</script>";
		} else {
			echo "Eroare: " . $conn->error;
		}
	} else {
		echo "Error: Numele Produsului prea scurt(MIN:4)";
	}
}
?>