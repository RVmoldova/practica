<!DOCTYPE html>
<html lang="ro">
<?php include 'header.php';?>
	<body>
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<form class="form-horizontal" name="regform" method="post" action="checkreg.php">
					<div class="form-group">
						<label for="regusername" class="col-sm-2 control-label">Porecla:</label>
						<div class="col-sm-10">
							<input name="regusername" Placeholder ="Introdu porecla" type="text" class="form-control" id="regusername">
						</div>
					</div>
					<div class="form-group">
						<label for="nume" class="col-sm-2 control-label">Nume:</label>
						<div class="col-sm-10">
							<input name="nume" Placeholder ="Introdu numele" type="text" class="form-control" id="nume">
						</div>
					</div>
					<div class="form-group">
						<label for="prenume" class="col-sm-2 control-label">Prenume:</label>
						<div class="col-sm-10">
							<input name="prenume" Placeholder ="Introdu prenumele" type="text" class="form-control" id="prenume">
						</div>
					</div>
					<div class="form-group">
						<label for="regpassword"  class="col-sm-2 control-label">Parola:</label>
						<div class="col-sm-10">
							<input name="regpassword" Placeholder ="Introdu parola" type="password" class="form-control" id="regpassword">
						</div>
					</div>
					<div class="form-group">
						<label for="datanasterii"  class="col-sm-2 control-label">Data nașterii:</label>
						<div class="col-sm-10">
							<input name="datanasterii" Placeholder ="YYYY-DD-MM" type="date" class="form-control" id="datanasterii">
						</div>
					</div>
					<input type="submit" class="btn btn-default" name="Submit" value="Inregistrare">
				</form>
			</div>
			<div class="col-md-3"></div>
		</div>
	</body>
</html>