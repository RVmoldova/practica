<?php
include 'connectDB.php';
if (elogat()) {
	$cum = $_SESSION['userid'];
	if (!isset($_GET['detailed'])) {
		echo "<h1><center>COMENZILE DUMNEAVOASTRĂ</center></h1>";
		$sq = "SELECT * FROM comanda WHERE idCumparator=$cum ORDER BY idComanda DESC";
		$i = 1;
		echo "<div class='row'><div class='col-md-1'></div><div class='col-md-10'><table class='table'><thead>";
		echo "<th>id Com.</th>";
		echo "<th>Data Comenzii</th>";
		echo "<th>Statut Comandă</th>";
		echo "</thead>";
		echo "<tbody>";
		if ($result = $conn->query($sq)) {
			while ($obj = $result->fetch_object()) {
				$idcom = $obj->idComanda;
				$data = $obj->dataComanda;
				$statut = $obj->procesat;
				echo "<tr class=\"chatrow clickableRow\" href=\"?tab=com&detailed=$idcom\">";
				echo "<td class=\"chatdata\" style=\" width: 5%; \">";
				echo $idcom;
				echo "</td>";
				echo "<td class=\"chatdata\">";
				echo $data;
				echo "</td>";
				echo "<td class=\"chatdata\">";
				echo $statut;
				echo "</td>";
				echo "</tr>";
				$i++;
			}
		}
		if ($i == 1) {
			echo "<tr style=\"background-color: #FF9F9F;\"><td>Nicio comandă</td><td></td><td></td></tr>";
		}
		echo "</tbody>";
		echo "</table>";
		echo "</div><div class='col-md-1'></div>";
	} else {
		$curcom = $_GET['detailed'];
		$sq = "SELECT * FROM comanda WHERE (idCumparator=$cum) and (idComanda=$curcom)";
		$result = $conn->query($sq);
		echo $conn->error;
		$obj = $result->fetch_object();
		$idcom = $obj->idComanda;
		$data = $obj->dataComanda;
		$statut = $obj->procesat;
		$tel = $obj->Telefon;
		$adr = $obj->adresa;
		echo "<h1><center>COMANDA NR.$idcom</center></h1>";
		echo "<div class='row'><div class='col-md-1'></div><div class='col-md-10'><table class='table'>";
		echo "<tr class='chatrow'>";
		echo "<td class='chatdata' style='width: 10%;'>Statut:</td><td class='chatdata'>$statut</td>";
		echo "</tr>";
		echo "<tr class='chatrow'>";
		echo "<td class='chatdata'>Data comenzii:</td><td class='chatdata'>$data</td>";
		echo "</tr>";
		echo "<tr class='chatrow'>";
		echo "<td class='chatdata'>Adresa:</td><td class='chatdata'>$adr</td>";
		echo "</tr>";
		echo "<tr class='chatrow'>";
		echo "<td class='chatdata'>Telefon:</td><td class='chatdata'>$tel</td>";
		echo "</tr>";
		echo "</table></div><div class='col-md-1'></div></div>";

		echo "<h3><center>CONȚINUTUL COMENZII:</center></h3>";
		$sq = "SELECT * FROM `continutcomanda`,`produse`,`producatori` WHERE `continutcomanda`.`idCom`=$idcom and `produse`.`idProdus`=`continutcomanda`.`prodId` and `produse`.`idProd`=`producatori`.`id`";
		$i = 1;
		echo "<div class='row'><div class='col-md-1'></div><div class='col-md-10'><table class='table'><thead>";
		echo "<th>Produs</th>";
		echo "<th>Preț unitar</th>";
		echo "<th>Cantitate</th>";
		echo "<th>Preț total</th>";
		echo "</thead>";
		echo "<tbody>";
		if ($result = $conn->query($sq)) {
			while ($obj = $result->fetch_object()) {
				$idprodus = $obj->idProdus;
				$produs = $obj->numeProdus;
				$pretun = $obj->pretProdus;
				$cant = $obj->cantitate;
				$producator = $obj->producatorNume;
				$total = $cant * $pretun;
				echo "<tr class=\"chatrow\">";
				echo "<td class=\"chatdata\" style=\" width: 70%; \">";
				echo "<a href=\"./index.php?tab=product&prid=$idprodus\">" . $producator . " " . $produs . "</a>";
				echo "</td>";
				echo "<td class=\"chatdata\">";
				echo $pretun;
				echo "</td>";
				echo "<td class=\"chatdata\">";
				echo $cant;
				echo "</td>";
				echo "<td class=\"chatdata\">";
				echo $total;
				echo "</td>";
				echo "</tr>";
				$i++;
			}
		}
		if ($i == 1) {
			echo "<tr style=\"background-color: #FF9F9F;\"><td>Nimic</td><td></td><td></td><td></td></tr>";
		} else {
			$sq = "SELECT SUM(`continutcomanda`.`cantitate`*`produse`.`pretProdus`) AS `suma` FROM `continutcomanda`,`produse` WHERE `continutcomanda`.`idCom`=$idcom and `produse`.`idProdus`=`continutcomanda`.`prodId`";
			$result = $conn->query($sq);
			$obj = $result->fetch_object();
			echo "<tr class=\"chatrow\">";
			echo "<td class=\"chatdata\">";
			echo "</td>";
			echo "<td class=\"chatdata\">";
			echo "</td>";
			echo "<td class=\"chatdata\" style=\"background-color: #FF9F9F;\">";
			echo "SUMA TOTALĂ:";
			echo "</td>";
			echo "<td class=\"chatdata\" style=\"background-color: #FF9F9F;\">";
			echo round($obj->suma, 2);
			echo "</td>";
			echo "<td class=\"chatdata\" style=\"background-color: #FF9F9F;\">LEI";
			echo "</td>";
			echo "</tr>";
		}
		echo "</tbody>";
		echo "</table>";
		echo "</div><div class='col-md-1'></div></div>";

	}
} else {
	echo "Insuficiente drepturi";
}
$conn->close();
?>