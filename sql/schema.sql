CREATE TABLE cititori 
  ( 
     idcit      NCHAR(100) PRIMARY KEY NOT NULL, 
     numecit    TEXT, 
     prencit    NCHAR(100), 
     sexcit     NCHAR(100), 
     studcit    NCHAR(100), 
     data_naste DATE, 
     adrcit     TEXT, 
     telcit     NCHAR(100) 
  ); 

CREATE TABLE carti 
  ( 
     idcarte NCHAR(100) PRIMARY KEY NOT NULL, 
     idatrib NCHAR(100), 
     stare   NCHAR(100) 
  ); 

CREATE TABLE comenzi 
  ( 
     idcom      NCHAR(100) PRIMARY KEY NOT NULL, 
     idcarte    NCHAR(100) NOT NULL, 
     idcit      NCHAR(100) NOT NULL, 
     dataimpr   NCHAR(100), 
     termzile   INT, 
     datarestit DATE, 
     FOREIGN KEY(idcarte) REFERENCES carti(idcarte), 
     FOREIGN KEY(idcit) REFERENCES cititori(idcit) 
  ); 

CREATE TABLE autori 
  ( 
     idautor   NCHAR(100) PRIMARY KEY NOT NULL, 
     numeautor NCHAR(100), 
     prenautor NCHAR(100), 
     secbiogr  TEXT 
  ); 

CREATE TABLE limbi 
  ( 
     idlimba  NCHAR(100) PRIMARY KEY NOT NULL, 
     denlimba NCHAR(100) 
  ); 

CREATE TABLE tematici 
  ( 
     idtem  NCHAR(100) PRIMARY KEY NOT NULL, 
     dentem NCHAR(100) 
  ); 

CREATE TABLE atribute 
  ( 
     idatrib  NCHAR(100) NOT NULL PRIMARY KEY, 
     idautor  NCHAR(100), 
     dencarte TEXT, 
     idtara   NCHAR(100), 
     aned     INT, 
     idtem    NCHAR(100), 
     idlimba  NCHAR(100), 
     numpag   INT, 
     pret     DECIMAL, 
     rezumat  TEXT, 
     FOREIGN KEY(idautor) REFERENCES autori(idautor), 
     FOREIGN KEY(idtara) REFERENCES tari(idtara), 
     FOREIGN KEY(idtem) REFERENCES tematici(idtem), 
     FOREIGN KEY(idlimba) REFERENCES limbi(idlimba) 
  ); 

CREATE TABLE tari 
  ( 
     idtara  NCHAR(100) PRIMARY KEY NOT NULL, 
     dentara NCHAR(100) 
  ); 