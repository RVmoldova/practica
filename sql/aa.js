//var url = "file:///C:/dev/experiments/crawl/bks/1-100.json";
var temp;
var bookDB = {};

var baseu = 'file:///C:/dev/experiments/crawl/bks/';

var urls = [
    '1-100.json',
    '1001-1100.json',
    '101-200.json',
    '1101-1500.json',
    '201-300.json',
    '301-400.json',
    '401-500.json',
    '501-600.json',
    '601-700.json',
    '701-800.json',
    '801-900.json',
    '901-1000.json'
];

function reqU(url) {
    var xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4) {
            console.log('done');
            temp = JSON.parse(xmlhttp.response);
            copynew();
        }
        //    console.log(xmlhttp.readyState + ' ' + xmlhttp.status);
    }

    xmlhttp.open("GET", url, true);
    xmlhttp.send();
}


function copynew() {
    for (table in temp) {
        if (!bookDB[table]) {
            bookDB[table] = {};
        }
        for (item in temp[table]) {
            bookDB[table][item] = temp[table][item];
        }
    }
};

function sendF() {
    var xmlhttp = new XMLHttpRequest(); // new HttpRequest instance 
    xmlhttp.open("PATCH", "https://transporta.firebaseio.com/.json");
    xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xmlhttp.send(JSON.stringify(bookDB));
}

urls.forEach(function(file) {
    reqU(baseu + file);
});
a = 0;
for (i in bookDB.author) {
    if (i.length > 70) {
        a++;
        delete bookDB.author[i];
    }
}

function sendFT() {
    var xmlhttp = new XMLHttpRequest(); // new HttpRequest instance 
    xmlhttp.open("PATCH", "https://transporta.firebaseio.com/tst.json");
    xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xmlhttp.send(JSON.stringify(bookDB.Lang));
}

["books", "author", "Lang", "theme"]
for (i in temp.Lang) {
    document.write('("' + i + '","' + temp.Lang[i].name + '"),<br/>');
}
for (i in temp.theme) {
    document.write('("' + i + '","' + temp.theme[i].name + '"),<br/>');
}

for (i in temp.author) {
    var n = temp.author[i].name.split('"').join('');
    var s = temp.author[i].surname.split('"').join('');
    document.write('("' + i + '","' + n + '","' + s + '"),<br/>');
}

for (i in a) {
    document.write('("' + a[i].code + '","' + a[i].name + '"),<br/>');
}

var tari = ["AF", "AX", "AL", "DZ", "AS", "AD", "AO", "AI", "AQ", "AG", "AR", "AM", "AW", "AU", "AT", "AZ", "BS", "BH", "BD", "BB", "BY", "BE", "BZ", "BJ", "BM", "BT", "BO", "BA", "BW", "BV", "BR", "IO", "BN", "BG", "BF", "BI", "KH", "CM", "CA", "CV", "KY", "CF", "TD", "CL", "CN", "CX", "CC", "CO", "KM", "CG", "CD", "CK", "CR", "CI", "HR", "CU", "CY", "CZ", "DK", "DJ", "DM", "DO", "EC", "EG", "SV", "GQ", "ER", "EE", "ET", "FK", "FO", "FJ", "FI", "FR", "GF", "PF", "TF", "GA", "GM", "GE", "DE", "GH", "GI", "GR", "GL", "GD", "GP", "GU", "GT", "GG", "GN", "GW", "GY", "HT", "HM", "VA", "HN", "HK", "HU", "IS", "IN", "ID", "IR", "IQ", "IE", "IM", "IL", "IT", "JM", "JP", "JE", "JO", "KZ", "KE", "KI", "KP", "KR", "KW", "KG", "LA", "LV", "LB", "LS", "LR", "LY", "LI", "LT", "LU", "MO", "MK", "MG", "MW", "MY", "MV", "ML", "MT", "MH", "MQ", "MR", "MU", "YT", "MX", "FM", "MD", "MC", "MN", "MS", "MA", "MZ", "MM", "NA", "NR", "NP", "NL", "AN", "NC", "NZ", "NI", "NE", "NG", "NU", "NF", "MP", "NO", "OM", "PK", "PW", "PS", "PA", "PG", "PY", "PE", "PH", "PN", "PL", "PT", "PR", "QA", "RE", "RO", "RU", "RW", "SH", "KN", "LC", "PM", "VC", "WS", "SM", "ST", "SA", "SN", "CS", "SC", "SL", "SG", "SK", "SI", "SB", "SO", "ZA", "GS", "ES", "LK", "SD", "SR", "SJ", "SZ", "SE", "CH", "SY", "TW", "TJ", "TZ", "TH", "TL", "TG", "TK", "TO", "TT", "TN", "TR", "TM", "TC", "TV", "UG", "UA", "AE", "GB", "US", "UM", "UY", "UZ", "VU", "VE", "VN", "VG", "VI", "WF", "EH", "YE", "ZM", "ZW"];
var temele = ["ScienceFiction", "Romance", "ShortStory", "Erotic", "Fantasy", "Thriller", "Drama", "MysteryCrime", "Humor", "Fiction", "JuvenileFiction", "History", "Religion", "Poetry", "StudyAids", "HealthFitness", "ComicsGraphicNovels", "LiteraryCollections", "Philosophy", "Esotericism", "Education", "Psychology", "Self-Help", "Adventure", "Cooking", "FairyTale", "Science", "SportsRecreation", "Horror", "Nature", "BiographyAutobiography", "LanguageLiterature", "Essay", "TrueCrime", "Medical", "FamilyRelationships", "Bibles", "Photography", "Design", "HouseHome", "AntiquesCollectibles", "Satire", "Art", "TechnologyEngineering", "Travel", "Music", "JuvenileNonfiction", "SocialScience", "BusinessEconomics", "Law", "LiteraryCriticism", "Pets", "Games", "Letters", "CraftsHobbies", "Computers", "Encyclopedia", "ForeignLanguageStudy", "PoliticalScience", "Mathematics", "PerformingArts", "Gardening", "Architecture"];

for (i in temp.books) {
    var title = temp.books[i].title.split('"').join('');
    var desc = temp.books[i].desc.split('"').join('');

    var randomtara = Math.floor((Math.random() * 243));
    var randomtema = Math.floor((Math.random() * 63));

    var write = '("';
    write += i;
    write += '","';
    write += temp.books[i].authid;
    write += '","';
    write += title;
    write += '","';
    write += temp.books[i].langid;
    write += '","';
    write += temp.books[i].wordsNR;
    write += '","';
    write += temp.books[i].price;
    write += '","';
    write += desc;
    write += '","';
    write += tari[randomtara];
    write += '","';
    write += temele[randomtema];
    write += '"),<br/>';
    document.write(write);
}

// authid: "-amy123"
// desc: "↵ dont you just love books well i can die for books i go to barns nd nobel everyday love it do you love that place?. "
// id: "_ebook-"
// langid: "English"
// price: 0.25
// title: "i love books dont you?"
// wordsNR: "25"


// CREATE TABLE atribute 
//   ( 
//      idatrib  NCHAR(100) NOT NULL PRIMARY KEY, 
//      idautor  NCHAR(100), 
//      dencarte TEXT, 
//      idtara   NCHAR(100), 
//      aned     INT, 
//      idtem    NCHAR(100), 
//      idlimba  NCHAR(100), 
//      numpag   INT, 
//      pret     DECIMAL, 
//      rezumat  TEXT, 
//      FOREIGN KEY(idautor) REFERENCES autori(idautor), 
//      FOREIGN KEY(idtara) REFERENCES tari(idtara), 
//      FOREIGN KEY(idtem) REFERENCES tematici(idtem), 
//      FOREIGN KEY(idlimba) REFERENCES limbi(idlimba) 
//   );
