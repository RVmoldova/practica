#include <fstream>
#include <iostream>
#include <cstdlib>
#include <conio.h>
#include <windows.h>
#include <utility>
#include <queue>


//////////////////////////////////
int n, m;
bool teren[52][52];
bool terenTemp[52][52];
int leeMap[52][52];
//////////////////////////////////


////////////////////////////////// PROCEDURA DE CITIRE
void citire() {
	std::ifstream fin("Teren.in");
	fin >> n >> m;
	for (int i = 1; i <= n; ++i) {
		for (int j = 1; j <= m; ++j) {
			fin >> teren[i][j];
		}
	}
	fin.close();
}
//////////////////////////////////


////////////////////////////////// PROCEDURA la EX1
void ex1() {
	system("cls");
	for (int i = 1; i <= n; ++i) {
		std::swap(teren[i][1], teren[i][m]);
	}
	std::cout << "prima si ultima coloana au fost interschimbate\n";
	getch();
}
//////////////////////////////////


//////////////////////////////////PROCEDURA la EX2
void ex2() {
	system("cls");
	std::cout << "deminam rind(r) sau coloana(c) ?\n";
	char optiuneAleasa = '\0';
	while (1) {
		optiuneAleasa = getch();
		if (optiuneAleasa == 'r' || optiuneAleasa == 'c') {
			break;
		} else {
			std::cout << "optiune inexistenta!\n";
		}
	}
	if (optiuneAleasa == 'r') {
		std::cout << "introduceti numarul rindului ce treb de deminat\n";
		int rind;
		while (1) {
			std::cin >> rind;
			if ((rind > 0) && (rind <= n)) {
				break;
			} else {
				std::cout << "introduceti un nr de la 1 pina la " << n << "\n";
			}
		}
		for (int i = 1; i <= m; ++i) {
			teren[rind][i] = 0;
		}
	} else {
		std::cout << "introduceti numarul coloanei ce treb de deminat\n";
		int coloana;
		while (1) {
			std::cin >> coloana;
			if ((coloana > 0) && (coloana <= m)) {
				break;
			} else {
				std::cout << "introduceti un nr de la 1 pina la " << m << "\n";
			}
		}
		for (int i = 1; i <= n; ++i) {
			teren[i][coloana] = 0;
		}
	}
	std::cout << "deminat cu succes!\n";
	getch();
}
//////////////////////////////////


//////////////////////////////////PROCEDURA la EX3
void ex3() {
	system("cls");
	int maximPeLinie, maximPeColoana, linia, coloana;
	maximPeLinie = maximPeColoana = linia = coloana = -1;

	for (int i = 1; i <= n; ++i) {
		int linietemp = 0;
		for (int j = 1; j <= m; ++j) {
			if (teren[i][j]) {
				linietemp++;
			}
		}
		if (linietemp > maximPeLinie) {
			maximPeLinie = linietemp;
			linia = i;
		}
	}

	for (int i = 1; i <= m; ++i) {
		int coloanatemp = 0;
		for (int j = 1; j <= n; ++j) {
			if (teren[j][i]) {
				coloanatemp++;
			}
		}
		if (coloanatemp > maximPeColoana) {
			maximPeColoana = coloanatemp;
			coloana = i;
		}
	}
	std::cout << "Maxime:\n";
	std::cout << "pe linia " << linia << " : " << maximPeLinie << "\n";
	std::cout << "pe coloana " << coloana << " : " << maximPeColoana << "\n";
	getch();
}
//////////////////////////////////


//////////////////////////////////PROCEDURA la EX4
void ex4() {
	system("cls");
	int sumaRinduri = 0, numarRinduri = 0;
	for (int i = 1; i <= n; ++i) {
		if (i % 2 == 1) {
			int cantTemp = 0;
			for (int j = 1; j <= m; ++j) {
				if (teren[i][j]) {
					cantTemp++;
				}
			}
			numarRinduri++;
			sumaRinduri += cantTemp;
		}
	}
	std::cout << "pe " << numarRinduri << " rinduri impare sunt " << sumaRinduri << " mine\n";
	std::cout << "Media: " << ((double)sumaRinduri / (double)numarRinduri) << " mine per rind\n";
	getch();
}
//////////////////////////////////


//////////////////////////////////PROCEDURA la EX5
void ex5() {
	system("cls");
	std::queue<int> cozi[52];
	for (int i = 1; i <= m; ++i) {
		int coloanatemp = 0;
		for (int j = 1; j <= n; ++j) {
			if (teren[j][i]) {
				coloanatemp++;
			}
		}
		cozi[coloanatemp].push(i);
	}
	for (int i = 50; i >= 1; --i) {
		if (cozi[i].size() > 0) {
			while (!cozi[i].empty()) {
				std::cout << "coloana " << cozi[i].front() << " -- " << i << " mine\n";
				cozi[i].pop();
			}
		}
	}
	getch();
}
//////////////////////////////////


//////////////////////////////////PROCEDURA la EX6
void ex6() {
	system("cls");
	std::ofstream fout("Mine.txt");
	for (int i = 1; i <= n; ++i) {
		int tempmine = 0;
		for (int j = 1; j <= m; ++j) {
			if (teren[i][j]) {
				tempmine++;
			}
		}
		if (tempmine > 0) {
			for (int j = 1; j <= m; ++j) {
				fout << teren[i][j] << " ";
			}
			fout << "\n";
		}
	}
	std::cout << "out in Mine.txt\n";
	fout.close();
	getch();
}
//////////////////////////////////


//////////////////////////////////PROCEDURA la EX7
void ex7() {
	struct fx {
		static void paint(int i, int j) {
			if (terenTemp[i][j]) {
				terenTemp[i][j] = 0;
				paint(i, j + 1);
				paint(i, j - 1);
				paint(i + 1, j);
				paint(i - 1, j);
				paint(i + 1, j + 1);
				paint(i + 1, j - 1);
				paint(i - 1, j + 1);
				paint(i - 1, j - 1);
			}
		}
	};
	system("cls");

	for (int i = 1; i <= n; ++i) {
		for (int j = 1; j <= m; ++j) {
			terenTemp[i][j] = teren[i][j];
		}
	}
	int obiecte = 0;
	for (int i = 1; i <= n; ++i) {
		for (int j = 1; j <= m; ++j) {
			if (terenTemp[i][j]) {
				obiecte++;
				fx::paint(i, j);
			}
		}
	}
	std::cout << "Pe teren sunt " << obiecte << " obiecte";
	getch();
}
//////////////////////////////////


//////////////////////////////////PROCEDURA la EX8
void ex8() {
	system("cls");
	struct fx {
		static void lee(int i, int j, int pas) {
			if ((i > 0) && (i <= n) && (j > 0) && (j <= m)) {
				if (!teren[i][j]) {
					if (leeMap[i][j] == 0 || leeMap[i][j] > pas) {
						leeMap[i][j] = pas;
						lee(i, j + 1, pas + 1);
						lee(i, j - 1, pas + 1);
						lee(i + 1, j, pas + 1);
						lee(i - 1, j, pas + 1);
					}
				}
			}
		}
		static void backLee(int i, int j, int pas) {
			if (pas > 1) {
				if (leeMap[i][j + 1] == pas - 1) {
					backLee(i, j + 1, pas - 1);
				} else if (leeMap[i][j - 1] == pas - 1) {
					backLee(i, j - 1, pas - 1);
				} else if (leeMap[i + 1][j] == pas - 1) {
					backLee(i + 1, j, pas - 1);
				} else if (leeMap[i - 1][j] == pas - 1) {
					backLee(i - 1, j, pas - 1);
				}
			}
			std::cout << pas << ". " << '[' << i << ',' << j << "]\n";
		}
	};
	fx::lee(1, 1, 1);
	if (leeMap[n][m] > 0) {
		std::cout << "calea cea mai scurta: \n";
		fx::backLee(n, m, leeMap[n][m]);
	} else {
		std::cout << "nu e posibil de ajuns\n";
	}
	getch();
}
//////////////////////////////////


//////////////////////////////////PROCEDURA de AFISARE
void afisare() {
	system("cls");
	for (int i = 1; i <= n; ++i) {
		for (int j = 1; j <= m; ++j) {
			std::cout << teren[i][j] << " ";
		}
		std::cout << "\n";
	}
	getch();
}
//////////////////////////////////



//////////////////////////////////PROCEDURA la MENIU
void menu() {
	int selectedOption = 1;
	while (1) {
		system("cls");
		std::cout << "\tMeniu:  ( w and s pentru selectare, q pentru quit)\n";
		std::cout << ((selectedOption == 1) ? "*" : " ") << "1. Interschimba in planul terenului prima cu ultima coloana\n";
		std::cout << ((selectedOption == 2) ? "*" : " ") << "2. Demina un rind/coloana\n";
		std::cout << ((selectedOption == 3) ? "*" : " ") << "3. Numarul maximal de mine pe linie/coloana\n";
		std::cout << ((selectedOption == 4) ? "*" : " ") << "4. Media zonelor minate de pe rindurile impare\n";
		std::cout << ((selectedOption == 5) ? "*" : " ") << "5. Ordinea descendenta a coloanelor dupa numarul de mine\n";
		std::cout << ((selectedOption == 6) ? "*" : " ") << "6. Afiseaza in Mine.txt doar liniile minate\n";
		std::cout << ((selectedOption == 7) ? "*" : " ") << "7. Numarul de obiecte\n";
		std::cout << ((selectedOption == 8) ? "*" : " ") << "8. Drumul cel mai scurt\n";
		std::cout << ((selectedOption == 9) ? "*" : " ") << "A. Afisare teren\n";
		std::cout << ((selectedOption == 10) ? "*" : " ") << "Q. Iesire\n";
		char pressedKey = getch();
		if (pressedKey == 'w') {
			selectedOption--;
			if (selectedOption < 1) {
				selectedOption = 10;
			}
		} else if (pressedKey == 's') {
			selectedOption++;
			if (selectedOption > 10) {
				selectedOption = 1;
			}
		} else if (pressedKey == 13) {
			if (selectedOption == 1) {
				ex1();
			}
			if (selectedOption == 2) {
				ex2();
			}
			if (selectedOption == 3) {
				ex3();
			}
			if (selectedOption == 4) {
				ex4();
			}
			if (selectedOption == 5) {
				ex5();
			}
			if (selectedOption == 6) {
				ex6();
			}
			if (selectedOption == 7) {
				ex7();
			}
			if (selectedOption == 8) {
				ex8();
			}
			if (selectedOption == 9) {
				afisare();
			}
			if (selectedOption == 10) {
				break;
			}
		}
		else if (pressedKey == 'q') {
			return;
		}
	}

}
//////////////////////////////////


//////////////////////////////////PROGRAMUL PRINCIPAL
int main() {
	citire();
	menu();

	return 0;

}
//////////////////////////////////
